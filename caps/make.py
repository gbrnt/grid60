#!/usr/bin/env python3
"""
Create STL files be extrude-cutting keycaps from `labels` subdir into keycaps
Export into `export` subdir
"""

import os
import sys
import glob


def getLabelList(label_dir="labels"):
    """
    Get a list of the labels present in the labels directory
    """

    all_label_paths = glob.glob(f"{label_dir}/*.svg")
    return [os.path.basename(l) for l in all_label_paths]


def cleanExportDir(export_dir="export", verbose=True):
    """
    Delete all files in the export directory
    """

    exported_files = glob.glob(f"{export_dir}/*")
    for f in exported_files:
        os.remove(f)
        if verbose:
            print(f"Deleted {f}")


def exportKey(key_label, export_dir="export", verbose=True):
    """
    Call OpenSCAD to export a single key STL
    """

    # Create export folder if it doesn't exist
    if not os.path.exists(export_dir):
        os.makedirs(export_dir)

    # Deal with label possibly ending in .svg, possibly not
    if key_label.endswith(".svg"):
        key_name = ".".join(key_label.split(".")[:-1])
    else:
        key_name = key_label
        key_label = f"{key_name}.svg"

    if not os.path.isfile(f"labels/{key_label}"):
        raise FileNotFoundError(f"Specified label {key_label} does not exist in `labels` directory")

    os.system(f"openscad -o export/{key_name}.stl -D 'key_label=\"{key_name}\"' generate_keys.scad")

    if verbose:
        print(f"Exported {key_name}.stl")


if __name__ == "__main__":
    # Get operation mode as first argument, and if not present assume all
    # Valid modes: "all", "clean", any key name present in `labels` directory
    try:
        mode = sys.argv[1]
    except IndexError:
        mode = "all"

    if mode == "clean":
        cleanExportDir()

    elif mode == "all":
        print("Exporting all keys")

        all_labels = getLabelList()

        for name in all_labels:
            exportKey(name)

    else:
        # Mode is something else - try to export specific keycap
        exportKey(mode)
