/*
 *  Generate labelled keys for grid60 keyboard
 */


// Constants
h_key_top_underside = 5.7;  // Text can't go below this height or there will be a hole
h_key_top_centre = 7.25;    // Extrusions above this won't touch the centre
h_label_start = 8;          // Label should start here and extrude downward
h_label_stop = h_key_top_centre - 0.4;  // This sets how far the label extrudes into the key
d_key = 18.2;
w_key = 18.2;

// Key label name - can be overridden in CLI
key_label_location = "labels/";
key_label = "1u_blank";


module Key(key_width_u) {
     /* Create an unmarked key with a given width */

     if (key_width_u == 1) {
          rotate([90, 0, 0]) import("DSA_1u.stl");
     }
     else if (key_width_u == 1.25) {
          rotate([90, 0, 0]) import("DSA_1.25u.stl");
          w_key = 22.95;
     }
}


module Label(file_location, key_label) {
     /* Create a key label from a given SVG file */

     translate([-w_key/2, -d_key/2, h_label_stop]) {
          linear_extrude(height=h_label_start-h_label_stop) {
                // Import actual label file
                import(str(file_location, key_label, ".svg"), center=false);
          }
     }
}


difference() {
     Key(1);
     Label(key_label_location, key_label);
}
