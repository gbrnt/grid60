# Grid60
A programmable keyboard with a 12x5 grid layout. The left and right edges are 1.25x width keys.

![Populated PCB with keycaps on the switches](docs/images/keyboard-caseless-top.jpg)

## PCB
![KiCad render of top side of PCB](docs/images/pcb-topside-render.png)
The PCB (developed in KiCad 5) has holes for 60 PCB-mount keys, although you could use it with plate mounted keys.
At the top left corner there is a micro USB port, oriented so the "hooks" of the cable face the back of the keyboard.
Most components are 0805 or larger.

There are three LEDs on the PCB for power (always on when 5V is applied), USB serial TX and USB serial RX.
The latter two are only really used when programming the chip, so they could easily be left unpopulated with almost no impact.

The crystal for the ATmega was supposed to be a surface mount crystal, but I wasn't paying attention and selected a through-hole footprint.
This is why the BOM has a link for a SMD HC49 crystal. Thankfully for that form factor, the pins can be straightened and it is back to being through-hole.

### Microcontroller
The microcontroller is an ATmega32U4-AU, and is soldered directly onto the PCB (i.e. not a separate Arduino board).
The -AU means that it is a QFP package, which is not too bad to hand-solder as long as you have some solder wick handy to remove excess solder.

Since the microcontroller won't have come from an Arduino you'll need to
[burn the bootloader](https://learn.sparkfun.com/tutorials/installing-an-arduino-bootloader/all) using the ICSP pins.
This will allow it to be programmed using `avrdude`.
You can test it without compiling QMK by programming an Arduino sketch which writes stuff over serial.

## Firmware
The firmware is QMK - the `firmware` directory contains the keyboard-specific firmware and a makefile to download and compile QMK.
The makefile isn't very good but it does the job for me.
It currently doesn't automatically program the keyboard, just generates a hex file.

## Case
![Keyboard case assembled](docs/images/keyboard-case-assembled.jpg)

The case is fairly basic and consists of 6 parts:

- Base-L - Left two thirds of the base
- Base-R - Right third of the base. Cut off to fit on the print bed of the Ender 3
- Top-L - Left side top clamp for the PCB
- Top-R - Right side top clamp for the PCB
- Reset-button - sits in Base-R part to allow pressing of the reset button
- Lightpipe - to make power LED visible

![Case parts](docs/images/keyboard-case-parts.jpg)

These parts are assembled with 5 M3x10mm pan head screws.
M3x8mm would work fine as well.
I personally prefer Pozi ones but hexalobular or hex socket would work fine too.
With the amount of disassembly and reassembly I would not recommend using Philips head screws as they are likely to be stripped.
