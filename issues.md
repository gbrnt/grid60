# To-do
## PCB
- [ ] Add mounting holes at corners
- [ ] Move diodes closer to switches to provide better space for case supports (and protect diodes from collisions)
- [ ] Move reset button to top side or use right-angle button on top edge
    - This would make it easier to get to instead of having to flip the keyboard over
- [ ] EMC fixes
    - [ ] Change both sides to ground planes
    - [ ] Add ground vias around edges
    - [ ] Ground unused pins? Risky - maybe use 0R links close to microcontroller?
    - [ ] See Ploopy mouse schematic for other ideas?
- [ ] Remove RX and TX LEDs - they don't really add functionality and are hidden in the case anyway
- [ ] Add some user-controllable LEDs (e.g. for caps lock or "game mode")
- [ ] Change to surface mount (and smaller?) crystal
- [ ] Add some underglow LEDs?
- [ ] Add sockets for key switches
    - Perhaps Mill-Max 0305-2-15-80-47-80-10-0, 7305-0-15-15-47-27-10-0 or 0279-0-15-01-47-27-10-0
    - Should probably adjust switch footprint for the sockets, to get hole diameter perfect
    - [See here for a comparison](https://www.reddit.com/r/MechanicalKeyboards/comments/cbykxw/millmax_socket_guide_pxlnght/)
- [ ] Use Akko CS Ocean Blue switches? Very cheap
- [ ] Make a wireless ZMK version using a Nice!Nano and 18650 holder

## Case
- [ ] PCB dimensions IRL don't quite seem to match the design - router took off slightly more than expected?
    - Really this should be fixed by locating using holes on the PCB, but they're not present
    - Could make entire case a few mm smaller, or make wedge pieces to fill in the side gaps
    - For now, just glued in
- [ ] The space for the PCB has been made too thick - it's not clamped by the top part even with some double sided tape attached
    - Using some glue or wedges works to hold it, but design could easily be altered to match PCB better
- [ ] Reset button is slightly too tall internally?
- [ ] Consider making reset button part of the case so it doesn't need to be printed as a separate part
- [ ] Add a switch plate to properly support the switches (probably required if using sockets)

## Caps
- [ ] Stems break - figure out stronger way to print or adjust dimensions
    - Possibly use PETG instead of PLA?
- [ ] Too labour-intensive to get smooth finish on top surface - investigate coating caps?
- [ ] Investigate marking legends without dirt-collecting gaps in surface - change colour mid layer?
