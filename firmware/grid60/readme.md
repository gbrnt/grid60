# gbrnt-12x5

12x5 ortholinear keyboard

Keyboard Maintainer: [George Bryant](https://gitlab.com/gbrnt)
Hardware Supported: Hand wired matrix, Arduino Pro Micro
Hardware Availability: Make it yourself!

Make example for this keyboard (after setting up your build environment):

    make gbrnt-12x5:default

See [build environment setup](https://docs.qmk.fm/build_environment_setup.html) then the [make instructions](https://docs.qmk.fm/make_instructions.html) for more information.
