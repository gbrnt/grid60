/* Copyright 2019 George Bryant
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

// Name layers for readability
#define _QWERTY 0
#define _PROG 1
#define _SYM 2
#define _FN 3

// Special keys
#define KC_XCAPE MT(MOD_LCTL, KC_ESC)
#define KC_SHSPC MT(MOD_LSFT, KC_SPC)
// Print screen and copy
#define KC_PSCC LCTL(LSFT(KC_PSCR))
// Windows workspaces
#define KC_WSPL LCTL(LWIN(KC_LEFT))
#define KC_WSPR LCTL(LWIN(KC_RIGHT))

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
// Base QWERTY layer
[_QWERTY] = LAYOUT(
  KC_ESC,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,     KC_8,    KC_9,    KC_0,    KC_DEL,    \
  KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,     KC_I,    KC_O,    KC_P,    TG(_PROG), \
  KC_LCTL, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,     KC_K,    KC_L,    KC_SCLN, KC_ENT,    \
  KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,     KC_COMM, KC_DOT,  KC_SLSH, KC_VOLU,   \
  KC_LCTL, MO(_FN), KC_RALT, KC_LALT, KC_LGUI, KC_SPC,  KC_BSPC, MO(_SYM), KC_MPRV, KC_MPLY, KC_MNXT, KC_VOLD    \
),
[_PROG] = LAYOUT(
  _______,  _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  _______,  _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  KC_XCAPE, _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  _______,  _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______ \
),
[_SYM] = LAYOUT(
  _______, KC_GRV,  _______, _______, _______, _______, _______, _______, _______, KC_LBRC, KC_RBRC, KC_HOME, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_END,  \
  _______, _______, _______, _______, _______, _______, _______, KC_MINS, KC_EQL,  KC_QUOT, KC_NUHS, _______, \
  _______, KC_NUBS, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______  \
),
[_FN] = LAYOUT(
  _______, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_PGUP, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_F11,  KC_F12,  KC_PGDN, \
  _______, _______, _______, KC_PSCR, KC_PSCC, _______, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, _______, _______, \
  _______, _______, _______, KC_WSPL, KC_WSPR, _______, _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_MSTP, _______, KC_MUTE  \
),
};

const uint16_t PROGMEM fn_actions[] = {

};

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
{
  // MACRODOWN only works in this function
      switch(id) {
        case 0:
          if (record->event.pressed) {
            register_code(KC_RSFT);
          } else {
            unregister_code(KC_RSFT);
          }
        break;
      }
    return MACRO_NONE;
};


void matrix_init_user(void) {

}

void matrix_scan_user(void) {

}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  return true;
}

void led_set_user(uint8_t usb_led) {

}
