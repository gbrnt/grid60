EESchema Schematic File Version 4
LIBS:grid60-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "Grid60 keyboard"
Date "2019-06-03"
Rev "A"
Comp "George Bryant"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3500 1700 1200 1150
U 5CF5BF1A
F0 "Microcontroller" 50
F1 "microcontroller.sch" 50
$EndSheet
$Sheet
S 5050 1700 1200 1150
U 5CF5BFB3
F0 "Keyboard matrix" 50
F1 "matrix.sch" 50
$EndSheet
$EndSCHEMATC
